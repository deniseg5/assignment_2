import ballerina/kafka;
import ballerina/internal;
import ballerina/io;
import ballerina/log;

@kubernetes:Deployment {
    image:"studentFile",
    name:"kafka-studentService"
}

endpoint kafka:consumerConfig consumer {
  bootstrapServers:"localhost:9091",
    groupId:"dsa assignment",
    topics:["assignment 3"],
    pollingInterval:400,
    autoCommit:false
};

listener kafka:Listener cons new (consumer)

service<kafka:Consumer> kafkaService bind consumer {

    resource function onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        foreach kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }

        var consumerAction = kafkaConsumer->commit();
		if (consumerAction is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", err = consumerAction);
    }

    function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
        byte[] serializedMsg = kafkaRecord.value;
        string msg = internal:byteArrayToString(serializedMsg,"UTF-8");

        io:println("New message received from the client");
        // Print the retrieved Kafka record.
        io:println("Topic: " + kafkaRecord.topic + " Received Message: " + msg);
        io:println("studentService has been written with the new File");
        io:println(studentFile.FILENAME.toString()+"\t"+studentFile.FILEPATH.toString()+"\t"+studentFile.FILESIZE.toString());
    }

}

