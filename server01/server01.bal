import ballerina/io;
import ballerina/log;
import ballerina/kafka;
import ballerina/internal;

@kubernetes:Deployment {
    image:"student-distributedFile",
    name:"kafka-studentService"
}

endpoint kafka:consumerConfig consumer {
    bootstrapServers:"localhost:9191",
    groupId:"dsa assignment",
    topics:["assignment 2"],
    pollingInterval:500,
    autoCommit:false
};

listener kafka:Listener cons new (consumer)

service<kafka:Consumer> kafkaService bind consumer {

    resource function onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
       
        foreach kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }
        // Commit offsets returned for returned records, marking them as consumed.
        consumerAction.commit();
        var consumerAction = kafkaConsumer->commit();
		if (consumerAction is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", err = consumerAction);
        }
    }

    function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
        byte[] serializedMsg = kafkaRecord.value;
        string msg = internal:byteArrayToString(serializedMsg,"UTF-8");
        // Print the retrieved Kafka record.
        io:println("Topic: " + kafkaRecord.topic + " Received Message: " + msg);
        foreach distributedFile in fileJson{
        io:println(distributedFile.FILENAME.toString()+"\t"+distributedFile.FILEPATH.toString()+"\t"+distributedFile.FILESIZE.toString());
    
    }
}