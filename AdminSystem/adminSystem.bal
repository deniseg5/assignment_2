import ballerina/kafka;
import ballerina/io;
import ballerina/h2;
import ballerina/mysql;

kafka:ControlProducer kafkaProducer = {
    // creating a producer configs with optional parameters client.id - used for broker side logging.
    bootstrapServers:"localhost:9092",
    clientID:"file_creation",
    acks:"all",  // number of acknowledgments for request complete,
    noRetries:3  // number of retries if record send fails.
};
//Endpoit for H2 Database
h2:Client testDB = {
    path: "./myAdmin",
    filePath: "testDB",
    username: "ADMIN",
    password: "",
    poolOptions: { maximumPoolSize: 3 }
};
function main (string... args) {
    io:println("The write operation:");
    var ret = testDB->write("CREATE TABLE DISTRIBUTEDFILE(FILENAME VARCHAR,
                     FILEPATH VARCHAR(255),FILESIZE BYTE, PRIMARY KEY (FILENAME))");
    handleWriteFile(ret, "Create distributedFile table");

    io:println("\nThe write operation - Inserting data to a distributedFile");
    ret = testDB->write("INSERT INTO distributedFile(fileName, filePath,fileSize)
                          values ('kafkaQuide.txt,'C:/Users/Sacky/Desktop/distributedStorageSystem','21MB')");
    handleWriteFile(ret, "Insert to distributedFile with no parameters");
    
    //Prompt user for a file details and insert into the distributedFile
    string filePath = io:readln("Enter file Path:");
    var  fileSize = <byte>io:readln("Enter file size:");
    byte resFlSize;
    match fileSize{
        byte resByte=>{
            resFlSize = resByte;
        }
        error e=>io:println("error:"+e.message);
    }

    var writeFil = testDB->write("INSERT INTO distributedFile(fileName, filePath,fileSize) values ("+resFlSize+",'"+filePath+"', 2)");
     handleWriteFile(writeFil, "Create distributedFile");

    //get items in distributedFile and send
    io:println("\nThe select operation - Select data from a distributedFile");
    var selectRet = testDB->select("SELECT * FROM distributedFile", ());
    distributedFile datatbl;
    match selectRet {
        distributedFile tableReturned => datatbl = tableReturned;
        error e => io:println("Select data from distributedFile distributedFile failed: "
                              + e.message);
    }
    //convert distributedFile to json and send to server
    io:println("\nConvert the distributedFile into json");
    var jsonConversionRet = <json>datatbl;
    json tableMsg;
    match jsonConversionRet {
        json jsonRes => {
            io:print("JSON: ");
            tableMsg = jsonRes;
            io:println(io:sprintf("%s", tableMsg));
        }
        error e => io:println("Error in distributedFile to json conversion");
    }
    io:println("TABLE DATA READY FOR SENDING:");
    io:println(io:sprintf("%s", tableMsg));

    byte[] serializedMsg = tableMsg.toString().toByteArray("UTF-8");
    kafkaProducer->send(serializedMsg, "dsa assignment", partition = 0);
    io:println("message sent to kafka...");

}
function handleWriteOperation(int|error returned, string message) {
    match returned {
        int retInt => io:println(message + " status: " + retInt);
        error e => io:println(message + " failed: " + e.message);
    }
}